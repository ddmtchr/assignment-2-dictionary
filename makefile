ASM=nasm
ASMFLAGS=-f elf64 -o
LD=ld
.PHONY: clean test

all: program

program: main.o dict.o lib.o
	$(LD) -o $@ $^
	
%.o: %.asm
	$(ASM) $(ASMFLAGS) $@ $<

main.o: main.asm words.inc lib.inc dict.inc

dict.o: dict.asm lib.inc colon.inc

lib.o: lib.asm

clean:
	rm -f *.o

test: program
	python3 test.py
