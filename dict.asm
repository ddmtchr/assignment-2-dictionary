%include "lib.inc"
%define KEY_OFFSET 8
%define VALUE_OFFSET 16

global find_word
section .text

; Ищет вхождение строки в ключи словаря. 
; Принимает указатель на строку, указатель на начало словаря.
; Возвращает адрес начала вхождения, если не найдено, то 0
find_word:
    push r12 ; сохранить callee-saved
    push r13
    mov r12, rdi ; переложить в них аргументы
    mov r13, rsi
.loop:
    mov rdi, r12 ; указатель на искомую строку в rdi
    mov rsi, [r13 + KEY_OFFSET]
    call string_equals ; сравнить на равенство ключ и искомое
    test rax, rax
    jne .success ; если равны - вхождение найдено
    mov r13, [r13] ; сместить текущий указатель
    test r13, r13 ; проверить указатель на след. элемент
    je .fail ; 0 - ключ не найден
    jmp .loop 
.fail:
    xor rax, rax
    jmp .end
.success:
    mov rax, r13 ; адрес вхождения в rax
.end:
    pop r13 ; вернуть callee-saved
    pop r12
    ret
