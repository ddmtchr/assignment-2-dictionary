import subprocess
import unittest

def run(input_data):
    nasm_executable = "./program"  
    try:
        process = subprocess.Popen(
            [nasm_executable],
            stdin=subprocess.PIPE,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            text=True,
        )

        stdout, stderr = process.communicate(input=input_data)
        return_code = process.returncode
        return stdout, stderr, return_code
    except Exception as e:
        return None, str(e), -1

class Tester(unittest.TestCase):

    def test_existing_key(self):
        input_data = "fifth_word"
        expected_output = "Fifth element\n"
        expected_error = ""
        stdout, stderr, return_code = run(input_data)
        self.assertEqual(return_code, 0, f"Error: Return code is {return_code}")
        self.assertEqual(stdout, expected_output, f"Error: Unexpected stdout: {stdout}")
        self.assertEqual(stderr, expected_error, f"Error: Unexpected stderr: {stderr}")

    def test_not_existing_key(self):
        input_data = "erognhibprtbnwt"
        expected_output = ""
        expected_error = "The search string was not found\\n"
        stdout, stderr, return_code = run(input_data)
        self.assertEqual(return_code, 1, f"Error: Return code is {return_code}")
        self.assertEqual(stdout, expected_output, f"Error: Unexpected stdout: {stdout}")
        self.assertEqual(stderr, expected_error, f"Error: Unexpected stderr: {stderr}")

    def test_very_long_key(self):
        input_data = "a" * 260
        expected_output = ""
        expected_error = "The string is too long\\n"
        stdout, stderr, return_code = run(input_data)
        self.assertEqual(return_code, 1, f"Error: Return code is {return_code}")
        self.assertEqual(stdout, expected_output, f"Error: Unexpected stdout: {stdout}")
        self.assertEqual(stderr, expected_error, f"Error: Unexpected stderr: {stderr}")



if __name__ == "__main__":
    test_suite = unittest.TestLoader().loadTestsFromTestCase(Tester)
    test_result = unittest.TextTestRunner(verbosity=2).run(test_suite)

    failed_tests = [test for test, _ in test_result.failures + test_result.errors]
    if failed_tests:
        print("Following tests failed:")
        for test in failed_tests:
            print(test._testMethodName)
    else:
        print("All tests are passed")
