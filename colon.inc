%define ptr 0
%macro colon 2
    %2: 
        dq ptr
        dq %%key
        dq %%value
    %%key:
        db %1, 0
    %%value:

    %define ptr %2
%endmacro
