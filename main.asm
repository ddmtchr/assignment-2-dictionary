%include "words.inc"
%include "lib.inc"
%include "dict.inc"
%define STDIN 0
%define STDOUT 1
%define STDERR 2
%define KEY_OFFSET 8
%define VALUE_OFFSET 16
%define BUFFER_SIZE 256

section .bss
buffer: resb BUFFER_SIZE

section .rodata
bufferof_msg:
    db "The string is too long\n", 0
not_found_msg:
    db "The search string was not found\n", 0

section .text
global _start

_start:
    mov rdi, buffer ; передаем в read_word ссылку на буфер
    mov rsi, BUFFER_SIZE ; и размер буфера
    call read_word
    test rax, rax ; если 0, то переполнение буфера
    je .bufferof
    mov rdi, rax ; передаем указатель на искомую строку в find_word
    mov rsi, ptr ; и указатель на начало словаря
    call find_word ; ищем слово
    test rax, rax ; если 0, то не найдено
    je .not_found
    mov rdi, [rax + VALUE_OFFSET] ; иначе печатаем, берем адрес начала значения
    call print_string
    call print_newline
    xor rdi, rdi ; код возврата 0
    call exit
.bufferof:
    mov rdi, bufferof_msg ; вывод сообщения о переполнении буфера
    jmp .err
.not_found:
    mov rdi, not_found_msg ; вывод сообщения о том, что строка не найдена
.err:    
    call print_error 
    mov rdi, 1 ; код возврата 1
    call exit
